resource "kubernetes_ingress" "ingress" {
  metadata {
    name = "vault-ingress"
  }

  spec {
    rule {
      http {
        path {
          backend {
            service_name = "vault-service"
            service_port = 8200
          }

          path = "/*"
        }
      }
    }
  }
}

