resource "kubernetes_service" "vault-service" {
  metadata {
    name = "vault-service"
  }
  spec {
    selector = {
      # App = kind_cluster.She-mvp.kind_config[0].node[0].App
    }
    port {
      port        = 8200
      target_port = 8200
    }

    type = "LoadBalancer"
  }
}
