terraform {
  required_providers {
    kind = {
      source  = "kyma-incubator/kind"
      version = "0.0.9"
    }

    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.5.0"
    }
  }
}



# for get nodes
# kubectl get nodes
# for delete nodes
# kubectl delete node <nodes-name>
# kind delete cluster --name kind-2